const tintColor = "#FF9052";

export default {
  tintColor,
  tabIconDefault: "#ccc",
  lucid_bgd: "#fbfbfb",
  tabIconSelected: tintColor,
  tabBar: "#fefefe",
  errorBackground: "red",
  errorText: "#fff",
  helpText: tintColor,
  warningBackground: "#EAEB5E",
  warningText: "#666804",
  noticeBackground: tintColor,
  noticeText: "#fff",
};
