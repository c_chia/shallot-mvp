import * as WebBrowser from "expo-web-browser";
import * as React from "react";
import Colors from "../constants/Colors";
import {
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { ScrollView } from "react-native-gesture-handler";

import { MonoText } from "../components/atoms/StyledText";

export default function HomeScreen() {
  return (
    <View style={styles.container}>
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainer}
      >
        <View style={styles.welcomeContainer}>
          <Image
            source={
              __DEV__
                ? require("../assets/images/shallot_icon.jpg")
                : require("../assets/images/robot-prod.png")
            }
            style={styles.welcomeImage}
          />
        </View>

        <View style={styles.learnMoreContainer}>
          <LearnMore />
        </View>
      </ScrollView>

      <View style={styles.tabBarInfoContainer}>
        <Text style={styles.bottomInfoText}>
          This is a bottom tab you can edit it in:
        </Text>

        <View
          style={[styles.codeHighlightContainer, styles.bottomColoredInfoText]}
        >
          <MonoText style={styles.codeHighlightText}>
            navigation/BottomTabNavigator.js
          </MonoText>
        </View>
      </View>
    </View>
  );
}

HomeScreen.navigationOptions = {
  header: null,
};

function LearnMore() {
  const learnMoreButton = (
    <Text onPress={handleLearnMorePress} style={styles.learnMoreLinkText}>
      Plus d'infos
    </Text>
  );

  return (
    <Text style={styles.aboutTitleText}>
      Shallot est une application qui permet de mettre en relation des
      particuliers volontaires pour faire les courses de leurs voisins.{" "}
      {learnMoreButton}
    </Text>
  );
}

function handleLearnMorePress() {
  WebBrowser.openBrowserAsync(
    "https://hackcovid19.bemyapp.com/#/projects/5e916a5756fbeb001b53d649"
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.noticeText,
  },
  aboutTitleText: {
    marginBottom: 20,
    color: "rgba(0,0,0,0.4)",
    fontSize: 14,
    lineHeight: 19,
    textAlign: "center",
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: "center",
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: "contain",
    marginTop: 3,
    marginLeft: -10,
  },
  learnMoreContainer: {
    alignItems: "center",
    marginHorizontal: 50,
  },
  codeHighlightText: {
    color: Colors.tintColor,
  },
  codeHighlightContainer: {
    backgroundColor: "rgba(0,0,0,0.05)",
    borderRadius: 3,
    paddingHorizontal: 4,
  },

  tabBarInfoContainer: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: "black",
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: "center",
    backgroundColor: Colors.lucid_bgd,
    paddingVertical: 20,
  },
  learnMoreLinkText: {
    fontSize: 14,
    color: Colors.helpText,
  },
  bottomInfoText: {
    fontSize: 17,
    color: Colors.tabIconDefault,
    textAlign: "center",
  },
  bottomColoredInfoText: {
    marginTop: 5,
  },
});
