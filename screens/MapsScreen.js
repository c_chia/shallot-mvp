import React from "react";
import { SafeAreaView, Text } from "react-native";
import MapView, { PROVIDER_GOOGLE } from "react-native-maps";

const MapsScreen = () => (
  /*<SafeAreaView>
    <Text>Screen: Home</Text>
  </SafeAreaView>*/
  <MapView
    style={{ flex: 1 }}
    provider={PROVIDER_GOOGLE}
    showsUserLocation
    initialRegion={{
      latitude: 37.78825,
      longitude: -122.4324,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
    }}
  />
);

export default MapsScreen;
