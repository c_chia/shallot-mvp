import * as React from "react";

import Colors from "../../constants/Colors";
import {
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";

export default function TabBarLabel(props) {
  return <Text style={styleBottomBar.textTab} />;
}

const styleBottomBar = StyleSheet.create({
  textTab: {
    color: Colors.tintColor,
  },
});
