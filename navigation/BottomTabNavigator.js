import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import * as React from "react";

import TabBarIcon from "../components/atoms/TabBarIcon";
import HomeScreen from "../screens/HomeScreen";
import ListScreen from "../screens/ListScreen";
import MapsScreen from "../screens/MapsScreen";
import AboutUsScreen from "../screens/AboutUsScreen";
import Colors from "../constants/Colors";
import TabBarLabel from "../components/atoms/TabBarLabel";

const BottomTab = createBottomTabNavigator();
const INITIAL_ROUTE_NAME = "Home";

export default function BottomTabNavigator({ navigation, route }) {
  // Set the header title on the parent stack navigator depending on the
  // currently active tab. Learn more in the documentation:
  // https://reactnavigation.org/docs/en/screen-options-resolution.html
  navigation.setOptions({ headerTitle: getHeaderTitle(route) });

  return (
    <BottomTab.Navigator initialRouteName={INITIAL_ROUTE_NAME}>
      <BottomTab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          title: "Mon Compte",
          //      tabBarLabel: ({ focused }) => <TabBarLabel focused={focused} />,
          tabBarIcon: ({ focused }) => (
            <TabBarIcon focused={focused} name="md-contact" />
          ),
        }}
      />

      <BottomTab.Screen
        name="MapsScreen"
        component={MapsScreen}
        options={{
          title: "Carte",

          tabBarIcon: ({ focused }) => (
            <TabBarIcon focused={focused} name="md-locate" />
          ),
        }}
      />

      <BottomTab.Screen
        name="ShoppingList"
        component={ListScreen}
        options={{
          title: "Liste de courses",

          tabBarIcon: ({ focused }) => (
            <TabBarIcon focused={focused} name="md-paper" />
          ),
        }}
      />

      <BottomTab.Screen
        name="AboutUs"
        component={AboutUsScreen}
        options={{
          title: "A Propos",

          tabBarIcon: ({ focused }) => (
            <TabBarIcon focused={focused} name="md-information-circle" />
          ),
        }}
      />
    </BottomTab.Navigator>
  );
}

function getHeaderTitle(route) {
  const routeName =
    route.state?.routes[route.state.index]?.name ?? INITIAL_ROUTE_NAME;

  switch (routeName) {
    case "Home":
      return "";
    case "MapsScreen":
      return "Carte";
    case "ShoppingList":
      return "Liste de Courses";
    case "AboutUs":
      return "A Propos";
  }
}
